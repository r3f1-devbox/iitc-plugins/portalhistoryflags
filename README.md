# portalHistoryFlags

## History-Modes

- [N] Normal (like intel)
- [M] Missing (Intel inverted, missing markers)
- [M-S] Like M, but without scout markers.
- [H] Highlighter Mode, so without any markers.

## Highlighter Mode

- Portals invisible when all markers activated
- orangeRed -> Portal not captured and not scouted
- orange -> Portal not scouted
- red -> Portal not captured
- Cyan -> all markers missing


## Tested

- IITC-CE
